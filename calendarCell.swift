//
//  calendarCell.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-03-11.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

import UIKit

class calendarCell: UITableViewCell {
    
    //Connects each view element to the cell

    
    @IBOutlet var eventLabel: UILabel!
    
    @IBOutlet var eventDate: UILabel!
    
    @IBOutlet var eventTime: UILabel!
    
    @IBOutlet var eventPlace: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
