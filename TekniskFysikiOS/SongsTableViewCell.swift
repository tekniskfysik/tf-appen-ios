//
//  SongsTableViewCell.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-02-20.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

/*
    Sets the properties of the cells for Songs AND Origo
*/

import UIKit

class SongsTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet var SongLabel: UILabel!
    
    @IBOutlet var origoNews: UILabel!
    @IBOutlet var identifierLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
