//
//  ShemaKartorKalenderViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-03-11.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

import UIKit

class SchemaKartorKalenderViewController: UIViewController, UITableViewDelegate  ,UITableViewDataSource{

    //Connects the TableView and navigation bar to the ViewController
    @IBOutlet var kalenderTableView: UITableView!
    
    @IBOutlet var schemaKartorKalenderNavItem: UINavigationItem!
    
    @IBAction func omButton(sender: AnyObject) {
        let ifObj = IFtext()
        let alertController = UIAlertController(title: "Om:", message:
            ifObj.ifMessage(), preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        

    }
    var pageIndex: Int!
    var titleText: String!
    
    //.ics parse parameters that's taken into account
    var dtstart = [String]()
    var dtend = [String]()
    var eventDescription = [String]()
    var location = [String]()
    var status = [Int] () //The statuses are treated as confirmed (1) or not (0)
    var summary = [String] ()
    
    //Variables that keep track wheter a parameter has been set
    //in an event or not. This way a default value can be set
    //so that the indexing of the .ics parse parameter arrays
    //is consistent, that is: Index i of each of these arrays
    //corresponds to the same event in the callendar.
    var dtstartSet: Int = 0
    var dtendSet: Int = 0
    var descriptionSet: Int = 0
    var locationSet: Int = 0
    var statusSet: Int = 0
    var summarySet: Int = 0
    
    //Counter that keeps track of the number of events
    var eventCounter:Int = 0

    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCallendar()
        
        
        kalenderTableView.delegate = self
        kalenderTableView.dataSource = self

        
      
        schemaKartorKalenderNavItem.title = self.titleText
        
        
        //Sets a RefreshControl so that the current page can be refreshed
        //by executing the function refresh(:AnyObject)
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Uppdatera flödet")
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        kalenderTableView.addSubview(self.refreshControl)
    }
    // Redos the URL requests and dismisses the
    // loading icon
    func refresh(sender:AnyObject)
    {
        loadCallendar()
        kalenderTableView.reloadData()
        self.refreshControl.endRefreshing()
    }

    
    
    //Loads the data from the given url, converts it into an array of strings
    //which is parsed into separate calendar events and assigns the values to
    //the view elements of the calendarCell
    func loadCallendar(){
        //semaphore is used to keep track of the running of the NSURLSession
        let semaphore = dispatch_semaphore_create(0)
        
        let requestCalendarURL: NSURL = NSURL(string: "https://www.google.com/calendar/ical/6ensdcblttc8o97tsii76tia1c%40group.calendar.google.com/public/basic.ics")!
        let calendarUrlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestCalendarURL)
        let session = NSURLSession.sharedSession()
        let calendarFill = session.dataTaskWithRequest(calendarUrlRequest) {
            
            (data, response, error) -> Void in
            if(response != nil){ // nil => no internet conection
                let httpResponse = response as! NSHTTPURLResponse
                let statusCode = httpResponse.statusCode
                
                
                //Code: 200 indicates a successfull download, see
                // p.39 at https://www.ietf.org/rfc/rfc2616.txt for further
                //reference
                if (statusCode == 200) {
                    
                    //"Translates" the data to a string array
                    let calendarDataToString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    var calendarArray = calendarDataToString!.componentsSeparatedByString("\r\n")
                    
                    var doLoop = true
                    var i = 0
                    
                    
                    var arrayElement: String
                    var splitArrayElement = [String] ()
                    
                    while(doLoop)
                    {
                        //The .ics file fetched by requestCalendarURL ends with
                        //END:VCALENDAR and even though the file might contain
                        //some stuff after this line it's of no interest to us
                        if(calendarArray[i] == "END:VCALENDAR"){
                            doLoop = false
                        }else if(calendarArray[i] == "BEGIN:VEVENT"){
                            i++ //We know that i isn't "END:VEVENT" so we might aswell step it up
                            while(calendarArray[i] != "END:VEVENT"){
                                
                                //Splits each line (element of the array) into two
                                //separate strings, one containing the string before
                                //the ":" and one containing the string after ":".
                                //If the string can't be splitted, i.e ":" is not
                                //found this has to be treated separately
                                arrayElement = calendarArray[i] as String!
                                splitArrayElement = arrayElement.componentsSeparatedByString(":")
                                if(splitArrayElement.count >= 2)//Makes sure the split did happen
                                {
                                    //The identifier of the current line is the
                                    //first element of the splitArrayElement
                                    //array
                                    let identifier = splitArrayElement[0]
                                    
                                    var content: String = ""
                                    
                                    //Creates the content string
                                    if(splitArrayElement.count > 2)
                                    {
                                        for(var j = 1;j < splitArrayElement.count; j++)
                                        {
                                            content += splitArrayElement[j]
                                        }
                                    }else{
                                        content = splitArrayElement[1]
                                    }
                                    
                                    //Multiline contents has to be treated in a
                                    //special way
                                    var tempArrayelement = calendarArray[i+1] as String!
                                    var tempSplit = [String] ()
                                    var stringContinues = true
                                    while (stringContinues)
                                    {
                                        tempSplit = tempArrayelement.componentsSeparatedByString(":")
                                        if(tempSplit[0] != "DTSTART" && tempSplit[0] != "DTEND" &&
                                            tempSplit[0] != "DESCRIPTION" && tempSplit[0] != "LOCATION"
                                            && tempSplit[0] != "STATUS" && tempSplit[0] != "SUMMARY"
                                            && tempSplit[0] != "DTSTAMP" && tempSplit[0] != "UID"
                                            && tempSplit[0] != "CREATED" && tempSplit[0] != "LAST-MODIFIED"
                                            && tempSplit[0] != "SEQUENCE" && tempSplit[0] != "TRANSP"
                                            && tempSplit[0] != "END"){
                                                //Drops the first char since it has proven to consist of
                                                //a blank space
                                                content += String(tempArrayelement.characters.dropFirst())
                                                i++
                                                tempArrayelement = calendarArray[i+1] as String!
                                        }else{
                                            stringContinues = false
                                        }
                                        
                                    }
                                    
                                    //Assigns the content to the proper element
                                    //in the array in accordance with the
                                    //found identifier
                                    if(identifier == "DTSTART"){
                                        self.dtstart += [content]
                                        self.dtstartSet = 1
                                    }else if(identifier == "DTEND"){
                                        self.dtend += [content]
                                        self.dtendSet = 1
                                    }
                                    if(identifier == "DESCRIPTION" && content != ""){
                                        self.eventDescription += [content]
                                        self.descriptionSet = 1
                                    }else if(identifier == "LOCATION" && content != ""){
                                        self.location += [content]
                                        self.locationSet = 1
                                    }else if(identifier == "SUMMARY" && content != ""){
                                        self.summary += [content]
                                        self.summarySet = 1
                                    }else if(identifier == "STATUS"){
                                        if(content == "CONFIRMED" && content != ""){
                                            self.status += [1]
                                        }else{
                                            self.status += [0]
                                        }
                                        self.statusSet = 1
                                    }
                                    i++
                                    
                                }else{ //No split did happen
                                    i++
                                }
                                
                            }//END WHILE !END:VEVENT
                            //IF dstartSet etc...
                            //Sets default values to parameters that were not set
                            //for the current event
                            if(self.dtstartSet == 0){
                                self.dtstart += ["Ingen tid satt"]
                            }
                            if(self.dtendSet == 0){
                                self.dtend += ["Ingen tid satt"]
                            }
                            if(self.descriptionSet == 0){
                                self.eventDescription += ["Det finns ingen beskrivning"]
                            }
                            if(self.locationSet == 0){
                                self.location += ["Ingen plats satt för detta event"]
                            }
                            if(self.statusSet == 0){
                                self.status += [0] //Defaults to unconfirmed status
                            }
                            if(self.summarySet == 0){
                                self.summary += ["Det finns ingen sammanfattning av eventet"]
                            }
                            
                            //Resets the variables keeping track of parameter
                            //occurance in preparation for the next event
                            self.dtstartSet = 0
                            self.dtendSet = 0
                            self.descriptionSet = 0
                            self.locationSet = 0
                            self.statusSet = 0
                            self.summarySet = 0
                            
                            //One event down, countless to go?
                            self.eventCounter++
                        }else{
                            i++
                        }
                    }
                } else{
                    self.eventCounter = 0
                }
                
            }else{
                self.eventCounter = 0
            }
            dispatch_semaphore_signal(semaphore);
        }
        //Runs the task calendarFill and then waits for it to finish
        calendarFill.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        
        /*
            The following code makes it possible to determine whether the event
            takes palce in the future or in the past and has been kept if
            one decides to implement a "display only future event" functionality
            in the future.
        */
        //let currentTime = NSDate()
        //let format = NSDateFormatter()
        //format.dateFormat = "yyyyMMdd"
        //        format.timeStyle = .ShortStyle
        //      format.dateStyle = .ShortStyle
        // var test1: String! = "20160312"
        // var ctS = format.stringFromDate(currentTime)
        // let myInt = Int(ctS)! - Int(test1)!
        //print("\(Int(ctS)!) - \(Int(test1))! = \(myInt)")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table View
    
    /*
    Sets up the Table View so that each cell can be filled with content
    dynamically.
    */
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //Sets the number of cells in each Table View depending on the number of events
    //and the settings saved in the apps default memory
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var idKey: String
        if self.eventCounter != 0
        {
            if(titleText == "Kalender")
            {
                idKey = "kalenderNumberOfCells"
                
                let defaults = NSUserDefaults.standardUserDefaults()
                if(defaults.integerForKey(idKey) > 0 && defaults.integerForKey(idKey) <= self.eventCounter){
                    return defaults.integerForKey(idKey)
                }else if(defaults.integerForKey(idKey) == 0 && self.eventCounter > 10){
                    return 10
                }else{
                    return self.eventCounter
                }
            }
          
        }
        
        return 1
    }
    
    //Sets the content of each cell
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cellIdentifier = "CalendarEventCell"
        let cell: calendarCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! calendarCell
    
       
        
        if(self.eventCounter != 0)
        {
            //Sets the way the event time is displayed
            var splitDtstart = [String] ()
            var splitDtend = [String] ()
            var timeString: String = "Ingen tid satt"
            var dateString: String
            splitDtstart = self.dtstart[indexPath.row].componentsSeparatedByString("T")
            splitDtend = self.dtend[indexPath.row].componentsSeparatedByString("T")
            if(splitDtstart.count == 2){
                if(splitDtend.count == 2){
                    splitDtstart[1] = timeParser(splitDtstart[1])
                    splitDtend[1] = timeParser(splitDtend[1])
                    
                    timeString = splitDtstart[1] + "-" + splitDtend[1]
                }
                
            }
            //Makes sure multiday events display properly
            if(splitDtstart[0] == splitDtend[0])
            {
                dateString = splitDtstart[0]
            }else{
                dateString = splitDtstart[0] + "-" + splitDtend[0]
            }
            
            self.location[indexPath.row] = cleanUpString(self.location[indexPath.row], opt: 0)
            
            cell.eventLabel.text = self.summary[indexPath.row]
            cell.eventTime.text = timeString
            cell.eventDate.text = dateString
            cell.eventPlace.text = self.location[indexPath.row]
            
        }else{
            cell.eventLabel.text = "Inget att visa"
            cell.eventTime.text = ""
            cell.eventDate.text = ""
            cell.eventPlace.text = ""
        }
        
        
        return cell
        
        
    }
    
    //Takes in a time stamp on the .ics format HHMMSSZ, with some sort of 
    //identifier (?) z at the end and returns a string formated as HH:MM
    func timeParser(var inString: String) -> String{
    
        inString =  String(inString.characters.prefix(2)) + ":" + String(inString.characters.suffix(inString.characters.count-2))
        
        inString = inString.substringToIndex(inString.endIndex.predecessor().predecessor().predecessor())
        
        return inString
    }
    
    //Cleans up a string so that any "\n" or "\" is removed, this is done by 
    //splitting the string into components around these two substrings and then
    //putting the components back together withtout the substrings. The opt
    //parameter constitutes an option whether the printout "\n" ought to be
    //replaced by the new line command \n.
    func cleanUpString(inString: String, opt: Int) -> String{
        var stringArray = [String] ()
        var outString = ""
        stringArray = inString.componentsSeparatedByString("\\n")
        for(var i = 0; i < stringArray.count; i++){
            if(opt == 0)
            {
                outString += stringArray[i]
            }else if(opt == 1){
                outString += "\r\n " + stringArray[i]
            }
        }
        stringArray = outString.componentsSeparatedByString("\\")
        outString = ""
        for(var i = 0; i < stringArray.count; i++){
            outString += stringArray[i]
        }
        
        return outString
    }
    
    
    //If the user selects one row (cell) of the Table View the details of the
    //event is displayed as given under the DESCRIPTION flag of the .ics file
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        
        self.eventDescription[indexPath.row] = cleanUpString(self.eventDescription[indexPath.row], opt: 1)
        let alertController = UIAlertController(title: "Event details:", message:
            self.eventDescription[indexPath.row], preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Ok!", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
        //Makes sure the taped cell is deselected so that it does NOT stay
        //highlighted
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
    }


}
