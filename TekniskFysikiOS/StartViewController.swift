//
//  StartViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-02-11.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//


import UIKit

class StartViewController: UIViewController, UIPageViewControllerDataSource{
    
    
    var pageViewController: UIPageViewController!
    var pageTitles: NSArray!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Sets the array of titles -> the number of different pages that will be
        //loaded in the Page View Controller
        self.pageTitles = NSArray(objects: "Nyheter","Twitter", "Instagram")
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("NyheterPageViewController") as! UIPageViewController
        

        
        self.pageViewController.dataSource = self
        
        //Sets initial View Controller, animation and position of the Page View 
        //Controllers
        let startVC = self.viewControllerAtIndex(0) as NyheterSocialtForumViewController
        let viewControllers = NSArray(object: startVC)
        
        
        self.pageViewController.setViewControllers((viewControllers as! [UIViewController]), direction: .Forward, animated: true, completion: nil)
        
        self.pageViewController.view.frame = CGRect(x: 0, y: 30, width: self.view.frame.width, height: self.view.frame.size.height-70)
        
        
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func viewControllerAtIndex(index: Int) -> NyheterSocialtForumViewController
    {
        if((self.pageTitles.count  == 0) || (index >= self.pageTitles.count))
        {
            return NyheterSocialtForumViewController()
        }
        
        let vc: NyheterSocialtForumViewController = self.storyboard?.instantiateViewControllerWithIdentifier("NyheterSocialtForumViewController") as! NyheterSocialtForumViewController
        
        vc.titleText = self.pageTitles[index] as! String
        vc.pageIndex = index
        
        return vc
        
    }
    
    
    // MARK: Page View Controller Data Source
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let vc =  viewController as! NyheterSocialtForumViewController
        var index =  vc.pageIndex as Int
        
        if (index == 0 || index == NSNotFound)
        {
            return nil
        }
        index--
        
        return self.viewControllerAtIndex(index)
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! NyheterSocialtForumViewController
        var index = vc.pageIndex as Int
        
        if(index == NSNotFound)
        {
            
            return nil
        }
        index++
        
        if(index == self.pageTitles.count)
        {
            return nil
        }
        
        return self.viewControllerAtIndex(index)
        
    }
    
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.pageTitles.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
    //motionEnded and dismissShakecausedAllert works together to make it
    //possible to shake the device and see random F quotes
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        if motion == .MotionShake {
            
            let quoteObj = quote()
            

            let alertController = UIAlertController(title: "CITAT by F", message:
                quoteObj.generateQuote(), preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok!", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
    }
    
}

