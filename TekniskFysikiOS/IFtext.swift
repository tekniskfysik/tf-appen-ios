//
//  IFtext.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-04-17.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

import Foundation

class IFtext {
    
    func ifMessage() ->String{
        let messageText: String =  "Teknisk fysik version 0.1 beta \n \n Utvecklare:\n\n Android: \n Karl Lundberg, F12 \n Axel Andersson, F12 \n\n iOS:\n Oskar Hallberg, F12"
     
        return messageText
    }
    
}