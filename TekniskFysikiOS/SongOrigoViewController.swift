//
//  SongOrigoViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-02-28.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

/*
    This class sets uses a Table View with two different cells and depending
    on whether Sånger or Origo is displayed the cell is changed accordingly
    so that different information can be displayed and different actions 
    performed if the user tap on one of the cells.
*/

import UIKit

class SongOrigoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    //Connects the Navigation Bar and Table View to the View Controller
    
    @IBOutlet var songOrigoNavBarItem: UINavigationItem!

    
    @IBOutlet var songTableView: UITableView!
    
    @IBAction func omButton(sender: AnyObject) {
        let ifObj = IFtext()
        let alertController = UIAlertController(title: "Om:", message:
            ifObj.ifMessage(), preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        

        
    }
    
    var songs = [String]()
    
    var songTexts = [String]()
        
    var pageIndex: Int!
    var titleText: String!
    
    var feed: RSSFeed?
    
    var refreshControl: UIRefreshControl!
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Number of sections in view
        return 1
    }
    
    //Sets the number of cells in the Table View. If the View Sånger is
    //displayed all songs are displayed, if the  View Origo is displayed
    //the number of items are determined by the saved settings for Origo and
    //the number of objects in the feed.
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var idKey: String
        if(titleText == "Sånger")
        {
            return songs.count
        }else{
            if let feed = self.feed
            {
             
                idKey = "origoNumberOfCells"
                    
                let defaults = NSUserDefaults.standardUserDefaults()
                if(defaults.integerForKey(idKey) > 0 && defaults.integerForKey(idKey) <= feed.items.count){
                    return defaults.integerForKey(idKey)
                }else if(defaults.integerForKey(idKey) == 0 && feed.items.count > 10){
                    return 10
                }else{
                    return feed.items.count
                }
            }
            
            return 1
            
        }
    }
    
    //The different views needs different cell heights
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if(titleText == "Sånger") //Songs
        {
            return 90
        }else{ //Origo
            return 110
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if(titleText == "Sånger"){
            //Loads the cell corresponding to Sånger and fills the cells
            //with each song title
            let cellIdentifier = "SongsTableViewCell"
            let cell: SongsTableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! SongsTableViewCell
        
            let song = songs[indexPath.row]
        
            cell.SongLabel.text = song
        
        
            return cell
        }else{
            //Loads the cell corresponding to Origo and fills it with the
            //information from the RSS feed
            let cellIdentifier = "OrigoTableViewCell"
            let cell: SongsTableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! SongsTableViewCell
            
            cell.identifierLabel.text = "O"
            
            if let feed = self.feed
            {
                let item = feed.items[indexPath.row] as RSSItem
                
                cell.origoNews.text = item.title
                
                let date = item.pubDate
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd/MM-yy"
                
                let Datestring = dateFormatter.stringFromDate(date!)
                
                cell.dateLabel.text = Datestring
                
            }else{ //If no feed available
                cell.origoNews.text = "Inget att visa"
                cell.dateLabel.text = ""
            }
            
            return cell
        }
    }
    
    
    /*
        Sets what will happen if the user selects a row (cell) in the Table 
        View. In the Sånger View a new View will be loaded containing the 
        corresponding song lyrics. In the Origo View the user is passed on
        to a Web View corresponding to the origin of the corresponding item
        in the feed.
    */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
       if(titleText == "Sånger")
       {
            let svc = self.storyboard?.instantiateViewControllerWithIdentifier("SongVC") as! SongTextViewController
            svc.Stext = songTexts[indexPath.row]
        svc.Stitle = songs[indexPath.row]
            presentViewController(svc, animated: false, completion: nil)
       }else{
        
        
        let svc = self.storyboard?.instantiateViewControllerWithIdentifier("NyheterSocialtForumWebViewController") as! NyheterSocialtForumWebViewController
        
        
        
        if let feed = self.feed
        {
            let item = feed.items[indexPath.row] as RSSItem
            svc.address = item.link!.absoluteString
            
        }
        
        svc.titleText = "Nyheter från Origo"
        
        presentViewController(svc, animated: false, completion: nil)
        
        
        

        }
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        songTableView.delegate = self
        songTableView.dataSource = self
        
        loadSongs()
        loadSongTexts()
        
        
        let request: NSURLRequest
    
        request = NSURLRequest(URL: NSURL(string: "http://www.ntk.umu.se/origo/feed/")!)
        
        
        RSSParser.parseFeedForRequest(request, callback: { (feed, error) -> Void in
            if let myFeed = feed
            {
                if let title = myFeed.title
                {
                    self.title = title
                }
                
                self.feed = feed
                self.songTableView.reloadData()
            }
        })

        
        
        songOrigoNavBarItem.title = self.titleText

        
        
        //Adds a refreshControl in the Origo view only
        if(titleText == "Origo"){

            self.refreshControl = UIRefreshControl()
            self.refreshControl.attributedTitle = NSAttributedString(string: "Uppdatera flödet")
            self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
            songTableView.addSubview(self.refreshControl)
        }
        
    }
    
    
    // Resets the data in the feed, redos the URL requests and dismisses the
    // loading icon
    func refresh(sender:AnyObject)
    {
        feed = nil
        
        let request: NSURLRequest
        
        request = NSURLRequest(URL: NSURL(string: "http://www.ntk.umu.se/origo/feed/")!)
        
        RSSParser.parseFeedForRequest(request, callback: { (feed, error) -> Void in
            if let myFeed = feed
            {
                if let title = myFeed.title
                {
                    self.title = title
                }
                
                self.feed = feed
                self.songTableView.reloadData()
            }
        })
        
        
        songTableView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    
    //Sets the song titles
    func loadSongs(){
        songs += ["Teknisk Fysiks Nationalsång"]
        songs += ["Bordeaux, Bordeaux"]
        songs += ["Bort allt vad oro gö"]
        songs += ["Dränkta Lucia"]
        songs += ["En liten enkel integral"]
        songs += ["Feta fransyskor"]
        songs += ["Fredmans epistel"]
        songs += ["Helan rasat"]
        songs += ["Härjarevisan"]
        songs += ["Jag har aldrig vart på snusen"]
        songs += ["Jag ska festa"]
        songs += ["Jag var full en gång"]
        songs += ["Kalmarevisan"]
        songs += ["Lille Olle"]
        songs += ["Min pilsner"]
        songs += ["Mitt lilla lån"]
        songs += ["O gamla klang och jubeltid"]
        songs += ["Porthos visa"]
        songs += ["Punschen kommer"]
        songs += ["Siffervisan"]
        songs += ["Spritbolaget"]
        songs += ["Strejk på Pripps"]
        songs += ["Teknisk fysik"]
        songs += ["Än en gång däran"]
        
    }
   
    //Sets the song texts corresponding to the song titles
    func loadSongTexts(){
        
        // Teknsik fysiks nationalsång
        songTexts += ["Melodi: Ryska federationens hymn\n\n" +
            "Vi är teknologer\n" +
            "därtill demagoger.\n" +
            "Vi är ju helt enkelt så otroligt bra.\n" +
            "Vi klarar det mesta\n" +
            "och gillar att festa\n" +
        "och tillhör eliten i vad vi än gör.\n\n" +
        "Refr: Vi är tekniska fysiker!\n" +
        "Vi är tekniska fysiker!\n" +
        "Låtom oss alla utbringa en skål!\n" +
        "Skål för oss alla fysiker!\n" +
        "Skål för oss alla fysiker!\n" +
        "Åh, vad vi älskar teknisk fysik!\n\n" +
        "Vi har kompetensen\n" +
        "och intelligensen.\n" +
        "Det finns ingenting som vi ej\n" +
        "klarar av.\n" +
        "Vi kan derivera\n" +
        "och sen integrera\n" +
        "och löser problem utav alla de\n" +
        "slag.\n\n" +
        "Refr."]
        
        //Bordeaux, Bordeaux
        songTexts += ["Melodi: I sommarens soliga dagar\n\n" +
            "Jag minns än idag hur min\n" +
            "fader\n" +
            "kom hem ifrån staden så glader\n" +
            "och ställde upp flaskor i rader\n" +
            "och sade nöjd som så:\n" +
            "Bordeaux, Bordeaux!\n\n" +
            "Han drack ett glas, kom i extas\n" +
            "och sedan blev det stort kalas.\n" +
            "Vi små glin, ja vi drack vin\n" +
            "som första klassens fyllesvin\n" +
            "och dansade runt där på bordet\n" +
            "och skrek så vi blev blå:\n" +
        "Bordeaux, Bordeaux!"]

        
        
        //Borta allt vad oro gör
        songTexts += ["Bort allt vad oro gör,\n\n" +
            "bort vad allt hjärtat kväljer.\n" +
            "Bäst att man väljer\n" +
            "bland dessa buteljer\n" +
            "sin maglikör.\n" +
            "Granne, gör du just som jag gör,\n" +
            "vet denna oljan ger humör.\n" +
            "Vad det var läckert!\n" +
            "Vad var det? Rhenskt Bläckert?\n" +
            "Oui, Monseigneur!\n\n" +
            "Bort allt vad oro gör,\n" +
            "allt är ju stoft och aska.\n" +
            "Låt oss bli raska\n" +
            "och tömma vår flaska\n" +
            "bland bröderna.\n" +
            "Granne, gör du just som jag gör,\n" +
            "vet denna oljan ger humör.\n" +
            "Vad det var mäktigt!\n" +
            "Vad var det? Jo, präktigt!\n" +
            "Mallaga - ja!\n\n" +
        "Text och musik: Carl Michael Bellman"]
        
        
        //Dränkta lucia
        songTexts += ["Melodi: Natten går tunga fjät\n\n" +
            "Huvet slår kopparslag\n" +
            "ögonen svider\n" +
            "magen i obehag\n" +
            "natten den lider\n" +
            "Då genom strupen går\n" +
            "hembränd en liten tår\n" +
            "vördat vare vårat brännvin\n" +
            "vörda vårt brännvin\n"]
        
        //En liten enkel integral
        songTexts += ["En liten enkel integral\n" +
            "ett vektoranalystal\n" +
            "ni har besväret,\n" +
            "ni har besväret att derivera.\n" +
            "Men tar man Stokes sats däruppå\n" +
            "så blir det så enkelt så\n" +
            "att integralen, att integralen\n" +
            "evaluera.\n\n" +
            "Och rotationen, den integreras\n" +
            "sen över ytan utav en boll,\n" +
            "koordinaterna transformeras,\n" +
            "så integranden blir bara noll.\n\n" +
            "En liten enkel integral\n" +
            "ett vektoranalystal\n" +
            "kan va så djävlig\n" +
            "att man ej hinner något mera.\n"]
        
        //Feta fransyskor
        songTexts += ["Melodi: Tomtarnas vaktparad\n\n" +
            "Feta fransyskor som svettas om \n" +
            "fötterna,\n" +
            "de trampa druvor som sedan ska \n" +
            "jäsa till vin.\n" +
            "Transpirationen viktig é,\n" +
            "ty den ge\n" +
            "fin bouquet.\n" +
            "Vårtor och svampar följer mé\n" +
            "men vad gör väl dé?\n\n" +
            "För vi vill ha vin, vill ha vin, vill ha \n" +
            "mera vin\n" +
            "även om följderna bli att vi må lida \n" +
            "pin\n" +
            "Flaskan och glaset gått i sin\n" +
            "Hit med vin, mera vin\n" +
        "Tror ni att vi är fyllesvin?"]
        
        
        //Fredmans epistel
        songTexts += ["Så lunka vi så småningom,\n" +
            "från Bacchi buller och tumult,\n" +
            "när döden ropar: Granne kom,\n" +
            "ditt timglas är nu fullt.\n" +
            "Du gubbe fäll din krycka ner,\n" +
            "och du, du yngling lyd min lag,\n" +
            "den skönsta nymf, som åt dig ler,\n" +
        "inunder armen tag.\n\n" +
        "Tycker du att graven är för djup,\n" +
            "nå välan så tag dig då en sup,\n" +
            "tag dig sen dito en,\n" +
            "dito två, dito tre\n" +
            "så dör du nöjdare.\n\n" +
            "Säg, är du nöjd, min granne säg,\n" +
            "så prisa världen nu till slut;\n" +
            "om vi ha en och samma väg,\n" +
            "så följoms åt; drick ut.\n" +
            "Men först med vinet rött och vitt,\n" +
            "för vår värdinna bugom oss,\n" +
            "och halkom sen i graven fritt,\n" +
        "vid aftonstjärnans bloss.\n\n" +
        "Tycker du att graven är för djup,\n" +
        "nå välan så tag dig då en sup,\n" +
        "tag dig sen dito en,\n" +
        "dito två, dito tre\n" +
        "så dör du nöjdare."]
        
        
        //Helan rasat
        songTexts += ["Melodi: Vintern rasat\n\n" +
            "Helan rasat ner i våra magar\n" +
            "skvalpar nu mot botten mol allen\n" +
            "I sin ensamhet den bittert klagar:\n" +
        "Det är inte lätt att va\' allen\n\n" +
        "Snart är halvan här den kära supen\n" +
        "alkoholiskt ren och silverklar\n" +
        "dansar som en vårbäck genom\n" +
        "strupen\n" +
        "Hamnar? Plask! I helans boudoir!"]
        
        
        //Härjarevisan
        songTexts += ["Liksom våra fäder, vikingarna i Norden.\n" +
            "Drar vi riket runt och super oss under borden.\n" +
            "Brännvinet har blitt ett elixir,\n" +
            "för kropp såväl som själ.\n" +
            "Känner du dig liten och ynklig på jorden,\n" +
            "växer du med supen och blir\n" +
            "stor ut i orden.\n" +
            "Slå dig för ditt håriga bröst och bli\n" +
            "en man från hår till häl.\n\n" +
            "Ja, nu skall vi ut och härja,\n" +
            "supa och slåss och svärja,\n" +
            "bränna röda stugor, slå små\n" +
            "barn och säga fula ord.\n" +
            "Med blod skall vi stäppen färga.\n" +
            "Nu äntligen lär ja\'\n" +
            "kunna dra någon riktig nytta\n" +
            "av min Hermodskurs i mord.\n\n" +
            "Hurra, nu ska man äntligen få\n" +
            "röra benen,\n" +
            "hela stammen jublar och det\n" +
            "spritter i grenen.\n" +
            "Tänk att än en gång få spränga\n" +
            "fram\n" +
            "på Brunte i galopp!\n" +
            "Din doft o kära Brunte är trots\n" +
            "sin brist i hygienen,\n" +
            "för en vild mongol minst lika ljuv\n" +
            "som syrenen.\n" +
            "Tänk att på din rygg få rida runt\n" +
            "i stan och spela topp.\n" +
            "Ja, nu skall vi ut och härja...\n\n" +
            "Ja, mordbränder är klämmiga ta\n" +
            "fram fotogenen\n" +
            "och eftersläckningen tillhör just\n" +
            "de fenomenen\n" +
            "inom brandmansyrket som jag\n" +
            "tycker\n" +
            "det är nån nytta med.\n" +
            "Jag målar för mitt inre upp den\n" +
            "härliga scenen\n" +
            "Blodrött mitt i brandgult, ej ens\n" +
            "prins Eugen en\n" +
            "lika mustig vy kan måla,\n" +
            "ens om han målade med sked.\n" +
            "Ja, nu skall vi ut och härja ...\n\n" +
        "Text: Hans Alfredsson och Levin\n"]
        
        
        //Jag har aldrig vart på snusen
        songTexts += ["Melodi: O hur saligt att få vandra\n\n" +
            "Jag har aldrig vart på snusen,\n" +
            "aldrig rökat en cigarr, halleluja!\n" +
            "Mina dygder äro tusen,\n" +
            "inga syndiga laster jag har.\n" +
            "Jag har aldrig sett nåt naket,\n" +
            "inte ens ett litet nyfött barn!\n" +
            "Mina blickar går mot taket,\n" +
            "därmed undgår jag frestarens\n" +
            "garn, halleluja!\n\n" +
            "Halleluja .....\n\n" +
            "Bacchus spelar på gitarren,\n" +
            "satan spelar på sitt\n" +
            "handklaver!\n" +
            "Alla djävlar dansar tango,\n" +
            "säg vad kan man väl önska\n" +
            "sig mer?\n" +
            "Jo, att alla bäckar vore\n" +
            "brännvin, Umeälven full av\n" +
            "bayerskt öl.\n" +
            "Konjak i varenda rännsten\n" +
            "och punsch i varendaste pöl,\n" +
            "mera öl!\n\n" +
        "Mera öl...."]
        
        
        //Jag ska festa
        songTexts += ["Melodi: Bamse\n\n" +
            "Jag ska festa, ta det lugnt med\n" +
            "spriten\n" +
            "ha det roligt utan att bli full\n" +
            "Inte krypa runt med festeliten\n" +
            "Ta det varligt för min egen skull\n\n" +
            "Först en öl i torra strupen\n" +
            "Efter det så kommer supen\n" +
            "I med vinet, ner med punschen\n" +
            "Sist en groggbuffe\n\n" +
            "Jag är skitfull, däckar först av alla\n" +
            "Missar festen, men vad gör väl de?\n" +
            "Blandar hejdlöst öl och gammal\n" +
            "filmjölk\n" +
            "Kastar upp på bordsdamen breve\'\n\n" +
            "Först en öl...\n\n" +
            "Spyan rinner ner på ylleslipsen\n" +
            "Raviolin torkar i mitt hår\n" +
            "Vem har lagt mig under\n" +
            "matssalsbordet\n" +
            "Vems är gaffeln i mitt högra lår\n"]
        
        
        //Jag var full en gång
        songTexts += ["Jag var full en gång för länge se'n\n" +
            "på knäna kröp jag hem\n" +
            "varje dike var för mig ett vilohem.\n" +
            "I varje hörn och varje vrå\n" +
            "hade jag en vän\n" +
        "ifrån renat upp till nittiosex procent.\n\n" +
        "Jag var full en gång för länge se\'n\n" +
        "på knäna kröp jag hem\n" +
        "och på vägen träffa jag en elefant. \n" +
        "Elefanten spruta vatten\n" +
        "och jag trodde det var vin,\n" +
        "sedan dess har alla kallat mig för \n" +
        "svin. Mera vin!\n\n" +
        "Jag var full en gång för länge se\'n\n" +
        "på knäna kröp jag hem\n" +
        "och på vägen träffa jag en elefant.\n" +
        "Elefanten spruta vatten\n" +
        "och jag trodde det var öhl,\n" +
        "sedan dess har alla kallat mig för \n" +
        "knöl. Mera öhl!\n\n" +
        "Jag var full en gång för länge se\'n\n" +
        "på knäna kröp jag hem\n" +
        "och på vägen träffa jag en elefant.\n" +
        "Elefanten spruta vatten\n" +
        "och jag trodde det var sprit,\n" +
        "sedan dess har alla kallat mig för \n" +
        "skit. Mera sprit!"]
        
        
        //Kalmarevisan
        songTexts += ["För uti Kalmare stad\n" +
            "ja där finns det ingen kvast\n" +
            "förrän lördagen.\n\n" +
            "Hej dick\n" +
            "Hej dack\n" +
            "Jag slog i\n" +
            "och vi drack\n" +
            "Hej dickom dickom dack\n" +
            "hej dickom dickom dack.\n\n" +
            "För uti Kalmare stad\n" +
            "ja där finns det ingen kvast\n" +
        "förrän lördagen.\n\n" +
        "/: När som bonden kommer\n" +
        "hem\n" +
        "kommer bondekvinnan ut :/\n" +
        "och är stor i sin trut\n\n" +
        "Hej dick...\n\n" +
        "/: Var är pengarna du fått?\n" +
        "Jo, dom har jag supit opp! :/\n" +
        "Uppå Kalmare slott.\n\n" +
        "Hej dick...\n\n" +
        "/: Jag skall mäla dig an\n" +
        "för vår kronbefallningsman :/\n" +
        "Och du skall få skam\n\n" +
        "Hej dick...\n\n" +
        "/: Kronbefallningsmannen vår\n" +
        "satt på krogen i går :/\n" +
        "Och var full som ett får.\n\n" +
        "Hej dick...\n\n" +
        "/: Säg var är din lab-rapport\n" +
        "Jo, den har jag supit bort! :/\n" +
        "För den var så kort!\n\n" +
        "Hej dick..."]
        
        
        //Lille Olle
        songTexts += ["Melodi: My Bonnie\n\n" +
            "Lille Olle skulle gå på disco\n" +
            "men han hade inte någon sprit\n" +
            "lille Olle skaffa’ lite hembränt\n" +
            "lille Olle gick då på en nit\n\n" +
            "Lille Olle skulle börja festa\n" +
            "spriten blanda’ han med äppelmer\n" +
            "lille Olle drack upp hela bålen\n" +
            "lille Olle ser nu inte mer\n\n" +
            "Lille Olle skaffade en ledhund\n" +
            "den var ful och även ganska trind\n" +
            "Olles ledhund drack en femton\n" +
            "flaskor\n" +
            "Olles ledhund är nu också blind\n\n" +
            "Lille Olle började med droger\n" +
            "blandade sin LSD med juice\n" +
            "Lille Olles hjärna stod i lågor\n" +
            "lille Olle dog av överdos\n\n" +
            "Lille Olle sitter nu i himlen\n" +
            "festa kan man göra även där\n" +
            "Lille Olle skaffade en ölback\n" +
        "capsar nu med Gud och Sankte Per!"]
        
        //Min pilsner
        songTexts += ["Melodi: My Bonnie\n\n" +
            "Min pilsner skall svalka min\n" +
            "tunga. Min pilsner skall duscha\n" +
            "min gom. Min pilsner skall få mig\n" +
            "att sjunga, om jag ser att flaskan\n" +
            "är tom: PILSNER! PILSNER!\n" +
            "Hämta en pilsner till mig, till mig.\n" +
            "PILSNER! PILSNER!\n" +
        "Hämta en pilsner till mig!"]

        
        //Mitt lilla lån
        songTexts += ["Melodi: Hej tomtegubbar\n\n" +
            "/: Mitt lilla lån det räcker inte\n" +
            "det går till öl och till brännvin. /:\n\n" +
            "Till öl och brännvin går det åt\n" +
            "och lite julmust emellanåt\n"]
        
        
        //O gamla klang och jubeltid
        songTexts += ["O, gamla klang och jubeltid,\n" +
            "ditt minne skall förbliva,\n" +
            "och än åt livets bistra strid\n" +
            "ett rosigt skimmer giva. \n" +
            "Snart tystnar allt vårt yra skämt,\n" +
            "vår sång blir stum,\n" +
            "vårt glam förstämt;\n\n" +
            "o, jerum, jerum, jerum,\n" +
            "o, quae mutatio rerum!\n\n" +
            "Var äro de som kunde allt,\n" +
            "blott ej sin ära svika,\n" +
            "som voro män av äkta halt\n" +
            "och världens herrar lika?\n" +
            "De drogo bort från vin och sång\n" +
            "till vardagslivets tråk och tvång;\n\n" +
            "o, jerum, jerum, jerum,\n" +
            "o, quae mutatio rerum!\n\n" +
            "filosofer:\n" +
            "den ene vetenskap och vett\n" +
            "in i scholares mänger,\n" +
            "jurister:\n" +
            "den andre i sitt anlets svett\n" +
            "på paragrafer vränger,\n" +
            "teologer:\n" +
            "en plåstrar själen som är skral,\n" +
            "medicinare:\n" +
            "en lappar hop dess trasiga fodral;\n\n" +
            "o, jerum, jerum, jerum,\n" +
            "o, quae mutatio rerum!\n\n" +
            "Men hjärtat i en sann student\n" +
            "kan ingen tid förfrysa.\n" +
            "Den glädjeeld, som där han\n" +
            "tänt,\n" +
            "hans hela liv skall lysa.\n" +
            "Det gamla skalet brustit har,\n" +
            "men kärnan finnes frisk dock\n" +
            "kvar, och vad han än må mista,\n" +
            "den skall dock aldrig brista.\n\n" +
            "Så sluten, bröder, fast vår krets\n" +
            "till glädjens värn och ära!\n" +
            "Trots allt vi tryggt och väl\n" +
            "tillfreds\n" +
            "vår vänskap trohet svära.\n" +
            "Lyft bägarn högt och klinga,\n" +
            "vän!\n" +
            "De gamla gudar leva än\n" +
            "bland skålar och pokaler,\n" +
        "bland skålar och pokaler!"]
        
        
        //Porthos visa
        songTexts += ["Jag vill börja gasqua,\n" +
            "var fan är min flaska,\n" +
            "vem i helvete stal min butelj.\n" +
            "Skall törsten mig tvinga,\n" +
            "en TT börja svinga,\n" +
            "men för fan bara blunda och svälj.\n" +
            "Vilken smörja,\n" +
            "får jag spörja,\n" +
            "vem fan tror att jag är en älg.\n\n" +
            "Till England vi rider\n" +
            "och sedan vad det lider,\n" +
            "träffar vi välan på någon pub.\n" +
            "Och där skall vi festa, blott dricka av det bästa\n" +
            "utav whisky och portvin,\n" +
            "ja tänker gå hårt in\n" +
        "för att smaka på rubb och stubb."]
        
        //Punschen kommer
        songTexts += ["Melodi: Vals ur Glada Änkan.\n\n" +
        "Punschen kommer,\n" +
        "punschen kommer\n" +
        "ljuv och sval.\n" +
        "Glasen imma,\n" +
        "röster stimma\n" +
        "i vår sal\n" +
        "Skål för glada minnen\n" +
        "Skål för varje vår,\n" +
        "Inga sorger finnas mer\n" +
        "när punsch vi får"]
        
        //Siffervisan
        songTexts += ["Melodi: Ritsch ratsch\n\n" +
            "1, 2, 75, 6, 7, 75, 6, 7, 75, 6, 7\n" +
            "1, 2, 75, 6, 7, 75, 6, 7, 73\n" +
            "107, 103, 102\n" +
            "107, 6, 19, 27\n" +
            "17, 18, 16, 15\n" +
            "13, 19, 14, 17\n" +
            "19, 16, 15, 11\n" +
            "8, 47\n"]
        
        //Spritbolaget
        songTexts += ["Melodi: Emil i Lönneberga \n\n" +
            "Till spritbolaget ränner jag, \n" +
            "och bankar på dess port. \n" +
            "Jag vill ha nå’t som bränner bra, \n" +
            "och får mig skitfull fort. \n" +
            "Expediten sade goda’, \n" +
            "hur gammal kan min herre va’. \n" +
            "Har du nåt leg, ditt fula drägg, \n" +
            "kom hit igen när du har fått skägg. \n\n" +
            "Nej, detta var ju inte bra, \n" +
            "jag skall bli full ikväll. \n" +
            "Då plötsligt en ide jag fick, \n" +
            "de har ju sprit på Shell. \n" +
            "Många flaskor stod där på ra’. \n" +
            "Så nu kan jag bli full och gla’. \n" +
            "Den röda drycken åkte ner, \n" +
        "nu kan jag inte se nå mer."]
        
        //Strejk på Pripps
        songTexts += ["Melodi: I natt jag drömde\n\n" +
            "I natt jag drömde något som, \n" +
            "jag aldrig drömt förut.\n" +
            "Jag drömde det var strejk på Pripps, \n" +
            "och alla ölen var slut.\n" +
            "Jag drömde om en jättesal\n" +
            "där ölen stod på rad.\n" +
            "Jag drack så där ett tjugotal \n" +
            "och reste mig och sa;\n \n" +
            "Armen i vinkel\n" +
            "blicken i skyn\n" +
            "så var det menat\n" +
            "whisky och renat\n" +
            "vårt mål alkohol!\n" +
        "För dem som tål! – SKÅL"]
        
        //Teknisk fysik
        songTexts += ["Melodi: Vårt land, vårt land (Finlands nationalsång)\n \n" +
            "Fysik, Fysik, Teknisk Fysik\n" +
            "Slå takt min pendelstång\n" +
            "Fysik, Fysik, Teknisk Fysik\n" +
            "Jag räknar och får fjong\n" +
            "Ej finns i hela Sveriges land\n" +
            "nåt så vackert som en integrand\n" +
            "Jag efter tentan tar min overall\n" +
        "och djärvt dikterar Skål!"]
        
        //Än en gång däran
        songTexts += ["Än en gång däran,\n" +
            "bröder, än en gång däran\n" +
            "Följom den urgamla seden\n" +
            "Intill sista man\n" +
            "Bröder, intill sista man\n" +
            "Trotsa vi hatet och vreden\n" +
            "Blankare vapen sågs aldrig i en\n" +
            "här\n" +
            "Än dessa glasen kamrater i\n" +
            "gevär\n" +
            "Än en gång däran\n" +
            "Bröder, än en gång däran\n" +
            "Svenska hjärtans djup här är\n" +
            "din sup\n\n" +
            "Livet är så kort,\n" +
            "Bröder, livet är så kort\n" +
            "Lek det ej bort, nej var redo\n" +
            "Kämpa mot allt torrt\n" +
            "Bröder, kämpa mot allt torrt\n" +
            "Tänk på de gamla som skredo\n" +
            "Fram utan tvekan i floder av\n" +
            "champagne\n" +
            "Styrkta från början av brännvin\n" +
            "från vårt land\n" +
            "Kämpa mot allt torrt\n" +
            "Bröder, kämpa mot allt torrt\n" +
            "Svenska hjärtans djup här är\n" +
            "din sup.\n\n" +
        "Text och musik: Evert Taube"]

        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
