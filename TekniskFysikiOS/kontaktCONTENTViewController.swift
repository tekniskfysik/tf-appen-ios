//
//  kontaktCONTENTViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-03-21.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

/*
    Connects the navigation bar to the view controller so that the title migth
    be set properly.

    This class sets the title of the view and connects each email address in the
    Kontakt tab to a button which then calls the standard email app and puts the
    corresponding address in the reciver field of a new email created in the 
    email app
*/

import UIKit

class kontaktCONTENTViewController: UIViewController {
    
    @IBOutlet var kontaktNavBarItem: UINavigationItem!
 
    @IBAction func progAnsvMail(sender: AnyObject) {
            let email = "maria.hamrin@umu.se"
            let url = NSURL(string: "mailto:\(email)")
            UIApplication.sharedApplication().openURL(url!)
        
    }
    
    @IBAction func bitrProgAnsvMail(sender: AnyObject) {
        
        let email = "krister.wiklund@umu.se"
        let url = NSURL(string: "mailto:\(email)")
        UIApplication.sharedApplication().openURL(url!)
    }
    
    @IBAction func studieVaglMail(sender: AnyObject) {
        let email = "lars-erik.svensson@umu.se"
        let url = NSURL(string: "mailto:\(email)")
        UIApplication.sharedApplication().openURL(url!)
        
    }
    
    
    @IBAction func itMail(sender: AnyObject) {
        let email = "it@tekniskfysik.se"
        let url = NSURL(string: "mailto:\(email)")
        UIApplication.sharedApplication().openURL(url!)
        
    }
    
    
    @IBAction func kvalMail(sender: AnyObject) {
        let email = "kvalitet@tekniskfysik.se"
        let url = NSURL(string: "mailto:\(email)")
        UIApplication.sharedApplication().openURL(url!)
    }
    
    @IBAction func samvMail(sender: AnyObject) {
        let email = "samverkan@tekniskfysik.se"
        let url = NSURL(string: "mailto:\(email)")
        UIApplication.sharedApplication().openURL(url!)
    }
    
    
    @IBAction func omButton(sender: AnyObject) {
        let ifObj = IFtext()
        let alertController = UIAlertController(title: "Om:", message:
            ifObj.ifMessage(), preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
    
    var titleText: String!
    var pageIndex: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        
         kontaktNavBarItem.title = self.titleText
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
