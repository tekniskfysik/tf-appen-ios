//
//  NyheterSocialtForumWebViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-03-08.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

import UIKit

class NyheterSocialtForumWebViewController: UIViewController, UIWebViewDelegate {
    
    //Connects the WebView to the View Controller and declares title and address
    //variables for later use
    @IBOutlet var webViewer: UIWebView!
    var address: String = ""
    var titleText: String!
    
    //Concects the WebView's navigation bar to the ViewController and declares
    //a url variable for later use
    @IBOutlet var webViewNavItem: UINavigationItem!
    var url:NSURL!
    
    //Back button that allows the user to return from the WebView to the 
    //ViewController
    @IBAction func backButton(sender: AnyObject) {
        
        let vc: UITabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("tabBar") as! UITabBarController
        presentViewController(vc, animated: false, completion: nil)
        vc.selectedIndex = 0
        
    }
    
    //Allows the same action as the back button to be performed on a
    //left-to-right swipe
    @IBAction func backSwipe(sender: AnyObject) {
        let vc: UITabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("tabBar") as! UITabBarController
        presentViewController(vc, animated: false, completion: nil)
        
        vc.selectedIndex = 0
    }
    
    
    //Allows the user to navigate back and forward between webpages in the 
    //WebView
    @IBAction func gobackWeb(sender: UIBarButtonItem) {
        if webViewer.canGoBack {
            webViewer.goBack()
        }
        
    }
    @IBAction func goforwardWeb(sender: UIBarButtonItem) {
        if webViewer.canGoForward {
        webViewer.goForward()
        }
        
        
    }
    
   var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(address != "")
        {
            url = NSURL(string: address)
        }else{
            
            url = NSURL(string: "")
        }
        
        let request = NSURLRequest(URL: url)
        
        //The .delegate =self setting allows the webView functions to run 
        //whenever the webview is to be loaded
        webViewer.delegate = self
        
        
        webViewer.loadRequest(request)

        
    
        webViewNavItem.title = self.titleText
        
        //Sets a RefreshControl so that the current page can be refreshed 
        //by executing the function refresh(:AnyObject)
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Uppdatera sidan")
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        webViewer.scrollView.addSubview(self.refreshControl)
        
        
        
        
    }
    
    //Updates the view in webViewr and dismisses the loading icon
    func refresh(sender:AnyObject)
    {
        webViewer.reload()
        self.refreshControl.endRefreshing()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Show the networkActivityIndicator while loading page
    func webViewDidStartLoad(webView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    //Hides the networkActivityIndicator when the page has loaded
    func webViewDidFinishLoad(webView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    //If the page fails to load this function makes sure that the user is
    //prompted with an error message
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false

        
        let alertController = UIAlertController(title: "Error -.-", message:
            "Failed to load: \(url)", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    //Clears the url whenever the WebView is dismissed
    //Note: Was added to fix a bugg that made it possible for the user to 
    //      get a random WebView loaded after they had left the WebView 
    //      environment.
    override func viewDidDisappear(animated: Bool) {
        address = ""
    }
    

}
