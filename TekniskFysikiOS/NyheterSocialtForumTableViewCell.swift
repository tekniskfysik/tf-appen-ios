//
//  NyheterSocialtForumTableViewCell.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-03-08.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

import UIKit

class NyheterSocialtForumTableViewCell: UITableViewCell {
    
    //Connects each view element to the cell
    
    @IBOutlet var TwitterLabel: UILabel!

    @IBOutlet var IdentifierLabel: UILabel!
    
    
    @IBOutlet var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
