//
//  NyheterSocialtForumViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-03-08.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

/*
    THE RSS IMPLEMENTATION IS AN ADAPTION OF SWIFTRSS GIVEN UNDER THE FOLLOWING
    LICENCE ON GITHUB:

The MIT License (MIT)

Copyright (c) 2014 Thibaut LE LEVIER

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import UIKit

class NyheterSocialtForumViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    //Connects the TableView and the navigation bar to the ViewController
    @IBOutlet var socialTableView: UITableView!

    @IBOutlet var nyheterSocialtForumNavBarItem: UINavigationItem!
    
    @IBAction func omButton(sender: AnyObject) {
        
        let ifObj = IFtext()
        let alertController = UIAlertController(title: "Om:", message:
            ifObj.ifMessage(), preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
    var pageIndex: Int!
    var titleText: String!
    
    var feed: RSSFeed?
    
    var refreshControl: UIRefreshControl!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        socialTableView.delegate = self
        socialTableView.dataSource = self
       
        let request: NSURLRequest
        
        
        //Depending on which page that is loaded the RSS feed is to be fetched
        //from different URLs.
        if(titleText == "Nyheter"){
            request = NSURLRequest(URL: NSURL(string: "https://developer.apple.com/news/rss/news.rss")!)
        }else if(titleText == "Twitter"){
         request = NSURLRequest(URL: NSURL(string: "https://twitrss.me/twitter_user_to_rss/?user=TekniskFysik")!)//https://twitrss.me/twitter_user_to_rss/?user=TekniskFysik
        }else{
            request = NSURLRequest(URL: NSURL(string: "http://tekniskfysik.se/instagramrss.php")!)
        }

        RSSParser.parseFeedForRequest(request, callback: { (feed, error) -> Void in
            if let myFeed = feed
            {
                if let title = myFeed.title
                {
                    self.title = title
                }
                
                self.feed = feed
                self.socialTableView.reloadData()
            }
        })

                nyheterSocialtForumNavBarItem.title = self.titleText
        
        
        
        //Sets a RefreshControl so that the current page can be refreshed
        //by executing the function refresh(:AnyObject)
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Uppdatera flödet")
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        socialTableView.addSubview(self.refreshControl)
    }

    // Resets the data in the cell, redos the URL requests and dismisses the
    // loading icon
    func refresh(sender:AnyObject)
    {
        feed = nil
        
        let request: NSURLRequest
        
        if(titleText == "Nyheter"){
            request = NSURLRequest(URL: NSURL(string: "https://developer.apple.com/news/rss/news.rss")!)
        }else if(titleText == "Twitter"){
            request = NSURLRequest(URL: NSURL(string: "https://twitrss.me/twitter_user_to_rss/?user=TekniskFysik")!) //https://twitrss.me/twitter_user_to_rss/?user=TekniskFysik
        }else{
            request = NSURLRequest(URL: NSURL(string: "http://tekniskfysik.se/instagramrss.php")!)
        }
        
        RSSParser.parseFeedForRequest(request, callback: { (feed, error) -> Void in
            if let myFeed = feed
            {
                if let title = myFeed.title
                {
                    self.title = title
                }
                
                self.feed = feed
                self.socialTableView.reloadData()
            }
        })

        
        socialTableView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Table View
    
    /*
        Sets up the Table View so that each cell can be filled with content
        dynamically.
    */
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //Sets the number of cells in each Table View depending on the feed on
    //the current page and the settings saved in the apps default memory
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var idKey: String
       if let feed = self.feed
        {
            if(titleText == "Nyheter")
            {
                idKey = "nyheterNumberOfCells"
            }else if(titleText == "Twitter"){
                idKey = "twitterNumberOfCells"
            }else{
                idKey = "instagramNumberOfCells"
            }
            
            
            let defaults = NSUserDefaults.standardUserDefaults()
            if(defaults.integerForKey(idKey) > 0 && defaults.integerForKey(idKey) <= feed.items.count){
                return defaults.integerForKey(idKey)
            }else if(defaults.integerForKey(idKey) == 0 && feed.items.count > 10){
                return 10
            }else{
                return feed.items.count
            }
            
        }
        
        return 1

    }
    
    //Sets the content of each cell
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
            let cellIdentifier = "TwitterCell"
            let cell: NyheterSocialtForumTableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! NyheterSocialtForumTableViewCell
        
            if(titleText == "Nyheter")
            {
                cell.IdentifierLabel.text = "N"
            }else if(titleText == "Twitter"){
                cell.IdentifierLabel.text = "T"
            }else{
                cell.IdentifierLabel.text = "I"
            }
        
            if let feed = self.feed
            {
                let item = feed.items[indexPath.row] as RSSItem
                
                cell.TwitterLabel.text = item.title
                
                let date = item.pubDate
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd/MM-yy"
                
                let Datestring = dateFormatter.stringFromDate(date!)

                cell.dateLabel.text = Datestring
                
            }else{
               cell.TwitterLabel.text = "Inget att visa"
                cell.dateLabel.text = ""
            }

        
            return cell
        
        
    }
    
    //If the user selects one row (cell) of the Table View the adress to the
    //corresponding item in the feed is loaded into a WebView and presented
    //to the user
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
            let svc = self.storyboard?.instantiateViewControllerWithIdentifier("NyheterSocialtForumWebViewController") as! NyheterSocialtForumWebViewController
        
        
            
            if let feed = self.feed
            {
                let item = feed.items[indexPath.row] as RSSItem
                svc.address = item.link!.absoluteString
             
            }
        
        if(titleText == "Nyheter"){
            svc.titleText = "Nyheter från Teknisk fysik"
        }else if(titleText == "Twitter"){
            svc.titleText = "Twitter/@TekniskFysik"
        }else{
            svc.titleText = "Instagram/@tekniskfysik"
        }
        
       

        
            presentViewController(svc, animated: false, completion: nil)

    }


}
