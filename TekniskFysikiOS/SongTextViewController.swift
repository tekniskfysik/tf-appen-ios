//
//  SongTextViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-02-21.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

/*
    Connects a Navigation Bar, Text View, a button and a swipe recognizer
    to the View Controller.

    The Navigation Bar and Text View are used to display the title and lyrics
    of the song the user chose in SongOrigo tab.
*/

import UIKit

class SongTextViewController: UIViewController {
   
    
    @IBOutlet var songText: UITextView!
  
    @IBAction func backSong1Button(sender: UIBarButtonItem) {
        let vc: UITabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("tabBar") as! UITabBarController
        presentViewController(vc, animated: false, completion: nil)
        vc.selectedIndex = 2
    }
    
    @IBAction func swipeBack(sender: AnyObject) {
        let vc: UITabBarController = self.storyboard?.instantiateViewControllerWithIdentifier("tabBar") as! UITabBarController
        presentViewController(vc, animated: false, completion: nil)
        vc.selectedIndex = 2
    }
    @IBOutlet var songTextNavItem: UINavigationItem!
    
    var Stext: String! //Lyrics
    var Stitle: String! //Title
    override func viewDidLoad() {
        super.viewDidLoad()
        songText.text = Stext
        songTextNavItem.title  = Stitle
        
        songText.textAlignment = .Center
        songText.font = UIFont(name: "Helvetica Neue", size: 18)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
