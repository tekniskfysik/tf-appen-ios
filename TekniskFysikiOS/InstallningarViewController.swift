//
//  InställningarViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-02-14.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

/*
    NOTE: Does not use the page view controller
*/


import UIKit

class InstallningarViewController: UIViewController {

    var pageViewController: UIPageViewController!
    var pageTitles: NSArray!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Loads the view and sets the title text.
        let svc = self.storyboard?.instantiateViewControllerWithIdentifier("InstallningarCONTENTViewController") as! InstallningarCONTENTViewController
        
        svc.titleText = "Inställningar"
        self.addChildViewController(svc)
        self.view.addSubview(svc.view)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //motionEnded and dismissShakecausedAllert works together to make it
    //possible to shake the device and see random F quotes
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        if motion == .MotionShake {
            
            let quoteObj = quote()
            
            let alertController = UIAlertController(title: "CITAT by F", message:
                quoteObj.generateQuote(), preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok!", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }

}
