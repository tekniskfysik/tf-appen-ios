//
//  NyheterViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-02-13.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

/*
    Connects the navigation bar to the ViewController and makes sure the
    titl text is displayed whenever the view is loaded.
*/

import UIKit

class NyheterViewController: UIViewController {
    
    
    @IBOutlet var startNavBarItem: UINavigationItem!
    
    var pageIndex: Int!
    var titleText: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        startNavBarItem.title = self.titleText
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
