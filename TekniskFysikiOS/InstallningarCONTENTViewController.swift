//
//  InställningarCONTENTViewController.swift
//  TekniskFysikiOS
//
//  Created by Oskar Hallberg on 2016-03-16.
//  Copyright © 2016 Oskar Hallberg. All rights reserved.
//

import UIKit

class InstallningarCONTENTViewController: UIViewController {
    
    //Outlets and actions connecting the various view elements to the source
    //code
    @IBOutlet var installningarNavBarItem: UINavigationItem!

    @IBOutlet var nyheterSlider: UISlider!
    @IBOutlet var nyheterSliderValue: UILabel!
    
    
    @IBOutlet var instagramSlider: UISlider!
    @IBOutlet var instagramSliderValue: UILabel!
    
    
    @IBOutlet var twitterSlider: UISlider!
    @IBOutlet var twitterSliderValue: UILabel!
    
    
    @IBOutlet var kalenderSlider: UISlider!
    @IBOutlet var kalenderSliderValue: UILabel!
    
    
    @IBOutlet var origoSlider: UISlider!
    @IBOutlet var origoSliderValue: UILabel!
    
    
    /* 
        The xxxSliderChanged IBAction functions overwrites the current value
        stored with the given key so that this value now is the only value
        availiable for a certain key. The writing to file is done everytime
        the value of the slider changes.

    */
    
    @IBAction func nyheterSliderChanged(sender: UISlider?) {
        let selectedValue = Int(sender!.value)
        
         nyheterSliderValue.text = String(stringInterpolationSegment: selectedValue)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(selectedValue, forKey: "nyheterNumberOfCells")
    }
    
    @IBAction func instagramSliderchanged(sender: UISlider) {
        let selectedValue = Int(sender.value)
        
        instagramSliderValue.text = String(stringInterpolationSegment: selectedValue)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(selectedValue, forKey: "instagramNumberOfCells")

    }
    
    @IBAction func twitterSliderChanged(sender: UISlider) {
        let selectedValue = Int(sender.value)
        
        twitterSliderValue.text = String(stringInterpolationSegment: selectedValue)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(selectedValue, forKey: "twitterNumberOfCells")
    }
    
    @IBAction func kalenderSliderChanged(sender: UISlider) {
        let selectedValue = Int(sender.value)
        
        kalenderSliderValue.text = String(stringInterpolationSegment: selectedValue)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(selectedValue, forKey: "kalenderNumberOfCells")
        
    }
    
    @IBAction func origoSliderChanged(sender: UISlider) {
        
        let selectedValue = Int(sender.value)
        
        origoSliderValue.text = String(stringInterpolationSegment: selectedValue)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(selectedValue, forKey: "origoNumberOfCells")
        
    }
    @IBAction func secretRotation(sender: UIRotationGestureRecognizer) {
        
        if(sender.state == .Began)//Prevents the rotation to be calle twice
        {
            let alertController = UIAlertController(title: "Hemligt citat:", message:
            "Get your s**t together! \n\n - K. L.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
        
            self.presentViewController(alertController, animated: true, completion: nil)
        
        }
        
        
    }
    
    @IBAction func omButton(sender: AnyObject) {
        let ifObj = IFtext()
        let alertController = UIAlertController(title: "Om:", message:
            ifObj.ifMessage(), preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func helpButton(sender: AnyObject) {
        let alertController = UIAlertController(title: "Hjälp", message:
            "Dra i reglagen för att ändra hur mycket som visas i respektive flöde.", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Ok!", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    
    var nyheterInt: Int!
    var instagramInt: Int!
    var twitterInt: Int!
    var kalenderInt: Int!
    var origoInt: Int!
    
    var pageIndex: Int!
    var titleText: String!


    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initialize the defaults values of the app
        let defaults = NSUserDefaults.standardUserDefaults()
        

        //Looks for settings corresponding to certain keys
        //Note: arbitrary keys, can be changed if neccessary
        nyheterInt = defaults.integerForKey("nyheterNumberOfCells")
        instagramInt = defaults.integerForKey("instagramNumberOfCells")
        twitterInt = defaults.integerForKey("twitterNumberOfCells")
        kalenderInt = defaults.integerForKey("kalenderNumberOfCells")
        origoInt = defaults.integerForKey("origoNumberOfCells")
        
        
        //If the apps is launched for the first time none of the above keys will
        //exists in memory and every setting will default to 0. To enchance the 
        //user experience on first launch the defaults are set to 10.
        if(nyheterInt == 0){
            nyheterInt = 10
        }
        if(instagramInt == 0){
            instagramInt = 10
        }
        if(twitterInt == 0){
            twitterInt = 10
        }
        if(kalenderInt == 0){
            kalenderInt = 10
        }
        if(origoInt == 0){
            origoInt = 10
        }
        
        
        //Displays the value of each setting in the corresponding label and
        //updates the position of each slider tracker to the corresponding value
        nyheterSliderValue.text = String(nyheterInt)
        nyheterSlider.setValue(Float(nyheterInt), animated: false)
        
        instagramSliderValue.text = String(instagramInt)
        instagramSlider.setValue(Float(instagramInt), animated: false)
        
        
        twitterSliderValue.text = String(twitterInt)
        twitterSlider.setValue(Float(twitterInt), animated: false)
        
        
        kalenderSliderValue.text = String(kalenderInt)
    
        kalenderSlider.setValue(Float(kalenderInt), animated: false)
        
        origoSliderValue.text = String(origoInt)
        origoSlider.setValue(Float(origoInt), animated: false)
        
        
        
        installningarNavBarItem.title = self.titleText

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
